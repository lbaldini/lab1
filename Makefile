exp_first = densita.tex dad_catenaria.tex pendolo_fisico.tex dad_palla.tex conducibilita_termica.tex dad_caduta.tex

all: macros latex1 latex2

latex1:
	pdflatex densita
	pdflatex dad_catenaria
	pdflatex pendolo_fisico
	pdflatex dad_palla
	pdflatex conducibilita_termica
	pdflatex dad_caduta
	pdflatex plasduino_cheatsheet

latex2:
	pdflatex pendolo_quadrifilare
	pdflatex ottica
	pdflatex dad_lente
	pdflatex oscillazioni_accoppiate
	pdflatex plasduino_cheatsheet

macros:
	cd macro; python translate.py

zip1:
	zip lab1_esercitazioni_primo_semestre.zip densita.pdf dad_catenaria.pdf pendolo_fisico.pdf \
		dad_palla.pdf conducibilita_termica.pdf dad_caduta.pdf plasduino_cheatsheet.pdf

zip2:
	zip lab1_esercitazioni_secondo_semestre.zip pendolo_quadrifilare.pdf \
		ottica.pdf dad_lente.pdf oscillazioni_accoppiate.pdf plasduino_cheatsheet.pdf

clean:
	rm -f *~ *.aux *.log *.out *.fls *.fdb_latexmk *.synctex.gz

cleanall:
	make clean; rm -f *.pdf *.zip
