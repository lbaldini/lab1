#! /usr/bin/env python

import glob
import os
import subprocess


DEFAULT_FILE_LIST = [
    'densita.py',
    'dad_catenaria.py',
    'pendolo_fisico.py',
    'dad_palla.py',
    'conducibilita_termica.py',
    'dad_caduta.py',
    'random_walk_hints.py'
    ]
LAB1_ROOT = os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir))
LAB1_CODE = os.path.join(LAB1_ROOT, 'code')


def pygmentize(file_path):
    """Run pygments on a python script and generate the corresponding
    LaTeX output.

    This is achieved through something along the lines of:
    pygmentize -f latex -O full -l python -o factorial_1.tex factorial_1.py
    """
    output_file_name = os.path.basename(file_path).replace('.py', '.tex')
    output_file_path = os.path.join(LAB1_CODE, output_file_name)
    cmd = f'pygmentize -f latex -O full -l python {file_path}'
    print(f'Running {cmd}...')
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
    # At this point we have the full pygmentized output available and we
    # do have to extract the relevant part.
    print(f'Generating output file {output_file_path}...')
    with open(output_file_path, 'w') as output_file:
        do_write = False
        for line in process.stdout.readlines():
            line = line.decode()
            if line.startswith('\\begin{Verbatim}'):
                do_write = True
            elif line.startswith('\\end{Verbatim}'):
                do_write = False
            if do_write:
                output_file.write(line)
        output_file.write('\\end{Verbatim}')
    print('Done, closing output file.')



if __name__ == '__main__':
    import sys
    if len(sys.argv) ==2:
        pygmentize(sys.argv[1], 'python')
    else:
        for file_name in DEFAULT_FILE_LIST:
            file_path = os.path.join(LAB1_ROOT, 'macro', file_name)
            pygmentize(file_path)
