\documentclass{lab1-article}

\title{Esercitazione sull'errore statistico}

\usepackage{fancyvrb}
\usepackage{hyperref}
\input{code/python}

\begin{document}


\begin{article}
\selectlanguage{italian}

\maketitle

\secintro
Lo scopo dell'esperienza \`e quello di familiarizzare con la stima e le
propriet\`a di base dell'errore statistico utilizzando un pendolo semplice.


\secmaterialsdad

\begin{itemize}
\item Filo e pesetto
\item Righello o metro a nastro
\item Cronometro digitale o \emph{smartphone}
\end{itemize}


\secdefinitions

Un pendolo semplice \`e tipicamente schematizzato come una massa puntiforme
appesa ad un punto fisso attraverso un filo inestensibile di massa nulla.

\begin{figure}[htb!]
  \begin{tikzpicture}[scale=1]
    \pgfmathsetmacro{\xc}{2.25}
    \pgfmathsetmacro{\yc}{0}
    \pgfmathsetmacro{\r}{4.5}
    \pgfmathsetmacro{\rangle}{1}
    \pgfmathsetmacro{\thetazero}{40}
    \node at (0, 0) {};
    \draw[style=densely dashed] (\xc, \yc) -- (\xc, \yc - \r);
    \draw (\xc, \yc) -- (\xc + \r*sin{\thetazero}, \yc - \r*cos{\thetazero});
    \draw[style=densely dashed] (\xc, \yc - \r) arc (270:270+\thetazero:\r);
    \draw[style=densely dashed] (\xc, \yc - \r*cos{\thetazero}) --%
    (\xc + \r*sin{\thetazero}, \yc - \r*cos{\thetazero});
    \draw (\xc, \yc - \rangle) arc (270:270+\thetazero:\rangle);
    \fill (\xc + \r*sin{\thetazero}, \yc - \r*cos{\thetazero}) circle%
          [radius=0.15];
    \node at (\xc - 0.25, 0) {$0$};
    \node[anchor=east] at (\xc , \yc - 0.5*\r) {$l\cos\theta_0$};
    \node[anchor=south] at%
    (\xc + 0.5*\r*sin{\thetazero}, \yc - \r*cos{\thetazero}) {$l\sin\theta_0$};
    \node at (\xc + 0.5*\r*sin{\thetazero} + 0.5, \yc - 0.5*\r*cos{\thetazero})%
          {$l$};
    \node at (\xc + \r*sin{\thetazero} + 0.5, \yc - \r*cos{\thetazero}) {$m$};
    \node at (\xc + 0.6, \yc - 1.3) {$\theta_0$};
  \end{tikzpicture}
  \caption{Schematizzazione dell'apparato sperimentale e definizioni di base.}
  \label{fig:pendolo}
\end{figure}

La lunghezza $l$ \`e la distanza tra il punto di sospensione ed
il centro di massa del pendolo stesso.
L'ampiezza di oscillazione $\theta_0$ \`e l'angolo formato dal filo con la
verticale all'inizio dell'oscillazione.
Il periodo di oscillazione $T$ \`e il tempo che il pendolo impiega a
compiere un'oscillazione completa.

L'espressione per il periodo del pendolo si pu\`o sviluppare in serie come
\begin{align}\label{eq:periodo_pendolo}
  T = 2\pi\sqrt{\frac{l}{g}} \left( 1 + \frac{1}{16}\theta_0^2 +
  \frac{11}{3072}\theta_0^4 + \cdots \right).
\end{align}
Nel limite di piccole oscillazioni solo il primo termine dello sviluppo
\`e importante ed il periodo \`e indipendente dall'ampiezza:
\begin{align}
  T = T_0 = 2\pi\sqrt{\frac{l}{g}}.
\end{align}


\secmeasurements

Realizzate un \emph{pendolo semplice} (per quanto praticamente possibile) con il
materiale che avete a disposizione a casa. Si consiglia di utilizzare una
lunghezza $l \sim 1$~m.

\labsubsection{Misure da effettuare}

Si misuri con il cronometro (o con lo \emph{smartphone} utilizzato come cronometro)
il tempo che il pendolo impiega a compiere 10 oscillazioni complete per
100--200~volte, e si prenda nota via via del valore registrato in un file di testo.


\labsubsection{Analisi dei dati}

Si utizzino i dati raccolti per stimare il periodo $T_0$ del pendolo nel limite
di piccole oscillazioni, e si confronti il risultato ottenuto con quanto
previsto dalla teoria, utilizzando il valore misurato di $l$.
(Per stimare il valore centrale del periodo misurato e l'incertezza associata
utilizzate la media aritmetica e la deviazione standard della media.)

Si calcolino la media aritmetica e la deviazione standard, del campione e della
media,
\begin{align}
  m = \frac{1}{n}\sum_{i=1}^n T_i \quad
  s^2 = \frac{1}{n-1}\sum_{i=1}^n (T_i - m)^2 \quad
  s_m = \frac{s}{\sqrt{n}}
\end{align}
su sottoinsiemi via via pi\`u grandi del campione totale, e.g., per le prime 5
misure, le prime 10, le prime 15 e cos\`i via, e si faccia un grafico
dell'andamento delle tre quantit\`a in funzione della dimensione $N$ del
sottocampione. Si commenti il risultato ottenuto. Ricordate:
\emph{la deviazione standard del campione $s$ rappresenta l'incertezza sulla
singola misura; la deviazione standard della media $s_m$ rappresenta l'incertezza
su $m$.}


\secconsiderations

\emph{Non utilizzate strumenti pericolosi, o con i quali non vi sentite
  perfettamente a vostro agio, nella realizzazione dell'esperienza. Non \`e
  necessario per la buona riuscita dell'eseperienza stessa!}

In questo caso l'incertezza sulla singola misura sar\`a determinata
principalmente dalle fluttuazioni del vostro tempo di reazione, e, presumibilmente,
sar\`a molto pi\`u grande della risoluzione del cronometro (i.e., quest'ultima
non gioca nessun ruolo nella misura.)

Per lo svolgimento dell'esercitazione dovete scegliere (e tracciare, mantenendola
costante) un'ampiezza iniziale di oscillazione. Abbiate cura di fare in
modo che questa sia abbastanza piccola da far in modo che ci\'o che
misurate sia effettivamente $T_0$, oppure correggete la vostra stima
utilizzando la~\eqref{eq:periodo_pendolo}.

Potrebbe essere utile appendere un foglio di carta dietro al vostro
pendolo per avere uno o pi\`u punti riferimento durante la misura, e.g., per
mantenere costante l'ampiezza iniziale di oscillazione e per facilitare
l'identificazione del passaggio nel punto pi\`u basso.

\onecolumn

\input{code/dad_errore_statistico}


\end{article}
\end{document}
