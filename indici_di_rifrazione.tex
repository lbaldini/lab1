\documentclass{lab1-article}

\title{Misure di indice di rifrazione}

\input{code/python}

\begin{document}


\begin{article}
\selectlanguage{italian}

\maketitle

\secsummary

L'obiettivo dell'esperienza consiste nella misura degli indici di rifrazione
del plexiglass e dell'acqua.


\secmaterials

\begin{itemize}
\item Banco ottico con sorgente luminosa;
\item un semicilindro di plexiglass;
\item un diottro sferico riempito di acqua;
\item un metro a nastro (risoluzione $1$~mm).
\end{itemize}


\secmeasurements


\labsubsection{Indice di rifrazione del plexiglass}

\begin{figure}[htb!]
  \begin{tikzpicture}[scale=1]
    \pgfmathsetmacro{\xc}{4.5}
    \pgfmathsetmacro{\yc}{0}
    \pgfmathsetmacro{\r}{2.25}
    \pgfmathsetmacro{\rthetaa}{0.7}
    \pgfmathsetmacro{\rthetab}{0.75}
    \pgfmathsetmacro{\l}{4}
    \pgfmathsetmacro{\thetain}{30}
    \pgfmathsetmacro{\thetarif}{20}
    \node at (0, 0) {};
    \draw (\xc, \yc) arc (90:270:\r);
    \draw (\xc, \yc)--(\xc, \yc-2*\r);
    \draw[style=densely dashed] (0, \yc - \r)--(8, \yc - \r);
    \draw (\xc, \yc-\r)--(\xc+\l*cos{\thetain}, \yc-\r+\l*sin{\thetain});
    \draw (\xc, \yc-\r)--(\xc-\l*cos{\thetarif}, \yc-\r-\l*sin{\thetarif});
    \draw (\xc+\rthetaa, \yc-\r) arc (0:\thetain:\rthetaa);
    \draw (\xc+\rthetab, \yc-\r) arc (0:\thetain:\rthetab);
    \draw (\xc-\rthetab, \yc-\r) arc (180:180+\thetarif:\rthetab);
    \node at (\xc+2*\rthetab*cos{\thetain},%
    \yc-\r+2*\rthetab*sin{\thetain/2}) {$\theta_{\rm i}$};
    \node at (\xc-2*\rthetab*cos{\thetarif},%
    \yc-\r-2*\rthetab*sin{\thetarif/2}) {$\theta_{\rm r}$};
    \node at (\xc+2*\rthetaa, \yc-1.5*\r) {$n_1 \sim 1$};
    \node at (\xc-1.25*\rthetaa, \yc-1.5*\r) {$n_2$};
  \end{tikzpicture}
  \caption{Schematizzazione dell'apparato per la misura dell'indice di
    rifrazione del plexiglass.
    L'angolo di incidenza $\theta_{\rm i}$ (di rifrazione $\theta_{\rm r}$)
    \`e l'angolo formato dal raggio luminoso incidente (rifratto) con la
    normale alla superficie di separazione tra i due mezzi.}
  \label{fig:plexiglass}
\end{figure}


Se un raggio di luce passa da un mezzo con indice di rifrazione $n_1$ ad
uno con indice di rifrazione $n_2$, gli angoli di incidenza e di rifrazione
sono legati tra di loro dalla legge di Snell
\begin{align}
  n_1 \sin\theta_{\rm i} = n_2\sin\theta_{\rm r}.
\end{align}

Si posizioni il semicilindro in modo che il raggio incida al centro della
superficie piana rifrangente (per evitare una seconda rifrazione in uscita) e
si misuri una serie di coppie $(\sin\theta_{\rm i},~\sin\theta_{\rm r})$ per un
certo numero (diciamo $10$) di valori di $\theta_{\rm i}$.
Si ricavi l'indice di rifrazione cercato da un fit lineare alle misure,
ricordando che l'indice di rifrazione dell'aria \`e con buona approssimazione
$n_1 \sim 1$.


\labsubsection{Indice di rifrazione dell'acqua}

\begin{figure}[htb!]
  \begin{tikzpicture}[scale=1]
    \pgfmathsetmacro{\xc}{2}
    \pgfmathsetmacro{\yc}{-1}
    \pgfmathsetmacro{\r}{2.25}
    \node at (0, 0) {};
    \draw (\xc, \yc) arc (180:360:\r);
    \fill (\xc+\r, 0) circle [radius=0.075];
    \node[anchor=west] at (\xc+\r, 0) {Sorgente};
    \fill (\xc+\r, \yc-1.75*\r) circle [radius=0.075];
    \node[anchor=west] at (\xc+\r, \yc-1.75*\r) {Immagine};
    \draw[style=densely dashed] (\xc+\r, 0)--(\xc+\r, \yc-1.75*\r);    
    \node at (\xc+\r+0.2, 0.5*\yc-0.5*\r) {$p$};
    \node at (\xc+\r+0.2, \yc-1.375*\r) {$q$};
    \node at (\xc, \yc-1.375*\r) {$n_1 \sim 1$};
    \node at (\xc+0.75, \yc-0.375*\r) {$n_2$};
  \end{tikzpicture}
  \caption{Schematizzazione dell'apparato per la misura dell'indice di
    rifrazione dell'acqua. Le grandezze $q$ e $p$ sono definite,
    rispettivamente come la distanza dal vertice del diottro della
    sorgente e dell'immagine (a fuoco sullo schermo).}
  \label{fig:acqua}
\end{figure}

Con riferimento alla figura~\ref{fig:acqua}, e detto $r$ il raggio del diottro,
$p$ e $q$ sono legati dalla relazione
\begin{align}\label{eq:diottro}
  \frac{n_2}{p} + \frac{n_1}{q} = \frac{(n_2 - n_1)}{r}.
\end{align}
%che, nel caso il diottro stesso sia immerso in aria ($n_1 \sim 1$) diviene
%\begin{align}\label{eq:diottro}
%  \frac{n_2}{p} + \frac{1}{q} = \frac{(n_2 - 1)}{r}.
%\end{align}

Operativamente, fissata una posizione per la sorgente, si muova lo schermo fino
a che l'immagine non \`e a fuoco, e si misurino $p$ e $q$. Si ripeta
l'operazione per diverse posizioni della sorgente e si costruisca il grafico
cartesiano di $1/q$ in funzione di $1/p$.
Ricordando che $n_1 \sim 1$, per la~\eqref{eq:diottro} le due grandezze saranno
legate da
\begin{align}
  \frac{1}{q} = -\frac{n_2}{p} + \frac{(n_2 - 1)}{r}.
\end{align}
Tramite fit lineare si stimi l'indice di rifrazione cercato come il
coefficiente angolare della retta di \emph{best fit}.


\secconsiderations

\labsubsection{Indice di rifrazione del plexiglass}

Troverete gi\`a montati sul banco ottico, accanto alla sorgente di luce, una
lente convergente ed un diaframma a fenditura per creare un fascio di luce
sottile. Non dovrebbe essere necessario modificare il montaggio---in caso di
bisogno chiedete aiuto all'esercitatore.


\labsubsection{Indice di rifrazione dell'acqua}

Come vedrete, la sorgente luminosa \`e immersa in acqua, per cui si raccomanda
di fare attenzione, durante gli spostamenti, onde evitare spiacevoli
fuoriuscite.

Si noti che l'oggetto da mettere a fuoco \`e un piccolo rombo incollato sulla
sorgente.


\secappendix{valori tabulati}

Si riportano di seguito i valori \emph{indicativi} degli indici di rifrazione
da misurare.

\medskip

\begin{center}
\begin{tabular}{p{0.65\linewidth}@{}p{0.3\linewidth}}
\hline
Materiale & $n$\\
\hline
\hline
Plexiglass & $1.48$\\
Acqua & $1.33$\\
\hline
\end{tabular}
\end{center}

\vfill

\noindent{\scriptsize Si veda il retro per un programma di esempio per
l'analisi dei dati con il calcolatore.\par}


\onecolumn

\input{code/refraction_index}

\end{article}
\end{document}
